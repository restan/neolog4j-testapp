package pl.edu.agh.neolog4j.testapp.sender;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class EventGenerator extends Observable implements Runnable {

	public void run() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		Random random = new Random();
		int j = 3;
		while (j > 0) {
			Integer i = random.nextInt(21) - 10;
			setChanged();
			notifyObservers(i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			j--;
			System.out.println("Event sent");
		}
		System.out.println("Sending finished");
	}
	
	public EventGenerator(Observer observer){
		addObserver(observer);
	}
}
