package pl.edu.agh.neolog4j.testapp.sender;

import java.util.Observable;
import java.util.Observer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EventObserver implements Observer{
	private static final Logger logger = LogManager.getLogger(EventObserver.class);
	private EventSender sender;

	public void update(Observable o, Object arg) {
		Integer i = (Integer) arg;
		logger.debug("Got {}.", i);
		if (i.intValue() == 0){
			logger.error("Found 0.");
		}
		String message = "New magic number: " + i.toString() + "!";
		sender.sendMessage(message, i);
	}
	
	public void setSender(EventSender sender) {
		this.sender = sender;
	}
}
