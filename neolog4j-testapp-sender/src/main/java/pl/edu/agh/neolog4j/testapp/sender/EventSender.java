package pl.edu.agh.neolog4j.testapp.sender;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

public class EventSender {
	private static final Logger logger = LogManager.getLogger(EventSender.class);

	private JmsTemplate jmsTemplate;
	private Destination destination;
	
	public void sendMessage(final String strMessage, final Integer magicNumber) {
		logger.debug("Message to send: \"" + strMessage + "\"."
				+ " Magic number = " + magicNumber.toString());
		MessageCreator messageCreator = new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				TextMessage message = session.createTextMessage(strMessage);
				message.setIntProperty("magicNumber", magicNumber);
                return message;
			}
		};
		jmsTemplate.send(destination, messageCreator);
		logger.debug("Message sent.");
	}

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}
}
