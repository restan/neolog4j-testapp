package pl.edu.agh.neolo4j.testapp.threads;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ThreadStarter {

	private static final Logger logger = LogManager.getLogger(ThreadStarter.class);
	
	public static void main(String[] args) {
		
		for (int i = 0; i < 4; i++) {
			logger.info("Creating thread TestThread_" + i);
			ThreadAction action = new ThreadAction();
			Thread thread = new Thread(action, "TestThread_" + i);
			thread.start();
			logger.info("TestThread_" + i + " started");
		}
	}
}
