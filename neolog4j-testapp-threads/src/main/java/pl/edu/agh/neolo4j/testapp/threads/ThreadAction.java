package pl.edu.agh.neolo4j.testapp.threads;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ThreadAction implements Runnable {

	private static final Logger logger = LogManager.getLogger(ThreadAction.class);
	
	public void run() {
		String name = Thread.currentThread().getName();
		Random random = new Random();
		logger.debug("Executing thread: " + name);
		try {
			int r = random.nextInt(1000) + 1000;
			logger.info("Sleep for " + r + " miliseconds.");
			Thread.sleep(r);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logger.debug("Tread " + name + " finished");
	}

}
