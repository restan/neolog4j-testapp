package pl.edu.agh.neolog4j.testapp.receiver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LetterCounter {
	private static final Logger logger = LogManager.getLogger(LetterCounter.class);
	private Map<Character, Integer> counter;
	
	public LetterCounter(){
		counter = new HashMap<Character, Integer>();
		String letters = "abcdefghijklmnopqrstuwvxyz";
		String bigLetters = letters.toUpperCase();
		for(char letter : (letters + bigLetters).toCharArray())
			counter.put(letter, 0);
	}
	
	public void countLetters(String text){
		List<Character> added = new ArrayList<Character>();
		for(Character character : text.toCharArray())
			if(Character.isLetter(character)){
				counter.put(character, counter.get(character));
				added.add(character);
			}
		logger.debug("Added in this call: " + added.toString());
	}
}
