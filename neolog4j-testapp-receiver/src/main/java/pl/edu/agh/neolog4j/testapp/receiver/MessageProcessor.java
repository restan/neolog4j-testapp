package pl.edu.agh.neolog4j.testapp.receiver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MessageProcessor {
	private static final Logger logger = LogManager.getLogger(MessageProcessor.class);
	private LetterCounter letterCounter;

	public void process(String message) {
		logger.debug("Message to process: " + message);
		letterCounter.countLetters(message);
	}
	
	public void setLetterCounter(LetterCounter letterCounter) {
		this.letterCounter = letterCounter;
	}
}
