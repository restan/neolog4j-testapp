package pl.edu.agh.neolog4j.testapp.receiver;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EventReceiver implements MessageListener{
	private static final Logger logger = LogManager.getLogger(EventReceiver.class);
	private MessageProcessor messageProcessor;

	public void onMessage(Message message) {
		System.out.println("Message received");
		if (message instanceof TextMessage) {
			try {
				String textMessage = ((TextMessage) message).getText();
				logger.info("Receive message: " + textMessage);
				messageProcessor.process(textMessage);
			} catch (JMSException ex) {
				logger.error(ex);
			}
		} else {
			logger.error("Message must be of type TextMessage");
		}
	}
	
	public void setMessageProcessor(MessageProcessor messageProcessor) {
		this.messageProcessor = messageProcessor;
	}
}
